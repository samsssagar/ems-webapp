import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../employee.service';

@Component({
  selector: 'app-comp-one',
  templateUrl: './comp-one.component.html',
  styleUrls: ['./comp-one.component.css']
})
export class CompOneComponent implements OnInit {

  employees:any[];

  selectedEmployees:[];

  constructor(private employeeService: EmployeeService){}

  ngOnInit(): void {
    this.employeeService
      .getEmployees()
      .subscribe(data => this.employees = data);
    this.selectedEmployees = [];
  }
  select = function(employee){
    //fetch the order with the orderId
    var index = this.employees.findIndex(e => employee.id === e.id);
    console.log(`The index is ${index}`);
    if (index >= 0){
      this.employees.splice(index, 1);
      this.selectedEmployees.push(employee);
    }
  }

  unselect = function(selectedEmployee){
    //fetch the order with the orderId
    console.log('inside the unselect method..')
    console.log(selectedEmployee)
    var index = this.selectedEmployees.findIndex(e => selectedEmployee.id === e.id);
    console.log(`The index is ${index}`);
    if (index >= 0){
      this.selectedEmployees.splice(index, 1);
      this.employees.push(selectedEmployee);
    }
  }
}
