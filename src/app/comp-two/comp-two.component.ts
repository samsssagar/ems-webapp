import { Component, OnInit } from '@angular/core';
import { EmployeeService} from '../employee.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-comp-two',
  templateUrl: './comp-two.component.html',
  styleUrls: ['./comp-two.component.css']
})
export class CompTwoComponent implements OnInit {

  constructor(private employeeService: EmployeeService, private route:Router){}

  ngOnInit(): void {

  }
  submitLeave = function(leaveForm){
    var leave = {doj:'',empName:'',empEmail:'',line_leaves:[]  };
    leave.doj=leaveForm.doj;
    leave.empName = leaveForm.empName;
    leave.empEmail=leaveForm.empEmail
    leave.line_leaves.push({'leaveReason': leaveForm.leaveReason,'comments':leaveForm.comments,'leaveStatus':leaveForm.leaveStatus,'startDate':leaveForm.startDate,'endDate':leaveForm.endDate})
    this.employeeService.placeLeaves(leave).subscribe(() => {
      this.route.navigate(['one']);
    });
  }
}
