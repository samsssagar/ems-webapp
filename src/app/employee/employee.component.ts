import { Component, Input, Output, EventEmitter, OnInit, OnChanges, OnDestroy, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent {
  @Input('data') employee;
  @Output('handleSelect') clicked = new EventEmitter();

  select = function($event){
    this.clicked.emit($event);
  }
  ngOnChanges(changes: SimpleChanges): void {
    console.log('inside the on changes method of Employee component')
    console.log(this.employee)
  }
}
